# Introduction

This plugin is a modification based on the "[ZenTao Open Source LDAP Plugin](https://github.com/TigerLau1985/ZenTao_LDAP)"
The current test version is **18.7**

## Features

1. Compatible with both local and LDAP user logins simultaneously
   - Please check if the `ldap` field exists in the `zt_user` table of the database in use. If not, you need to add a script in the `db` directory. For details, refer to the _Directory Structure_ section in the [ZenTao Secondary Development Manual](https://www.zentao.net/book/api/144.html)
   - <span style='color:red' >The `admin` user will also be overwritten. When importing data into LDAP that includes an `admin` account, remember to backup the original data first</span>
2. Supports default grouping function, with data landing in the DB (`zt_usergroup` table)
3. Added configuration for protocol and port
4. Added saving of mobile phone data

## Instructions

Reference documents:

- [Menu Configuration](https://www.zentao.net/book/zentaopmshelp/68.html#3)

### Usage

You need to package the `ldap` directory into a zip file, or directly execute the script `package.sh` in the current directory

1. Install the extension
2. Configure the extension settings to connect to ldap
3. Copy the saved extension settings from `ldap/config.php` to `user/ext/config/config.php`.
   This is necessary due to a bug in the program that prevents `model/identify.php` from reading
   from `ldap/config.php`.
   ```
   cat extension/custom/ldap/config.php | sed '1d' >> extension/custom/user/ext/config/config.php
   ```

### Menu Configuration

The _LDAP_ menu is mounted under `Admin->System Settings->LDAP`, belonging to the module menu. The corresponding configuration is `$lang->admin->menuList->system['subMenu']['ldap']`

Currently, the menu configuration is under the `admin` module. If you need to modify it, just change it to the corresponding module,
EX: `$lang->admin->menuList->company['subMenu']['xxxx'] => array('link' => "{$lang->xxxx->common}|xxxx|index|", 'subModule' => 'xxxx');`

The above code means that under the `admin` module, modify its submodule menu, the modified module is `company`, and a new module named `xxxx` is mounted, pointing to its own module

### Code Modification

If you want to modify/add your own data, such as adding a `dingding`/`weixin` field
The place to modify is:

1. In `ldap/model.php` within the `sync2db` method, modify the `$user` object as needed, e.g., `$user->dingding = xxxx`

### Configuration Example

| Option                  | Example Value             | Required | Default Value   |
| ----------------------- | ------------------------- | -------- | --------------- |
| Basic Configuration     |                           |          |                 |
|                         |                           |          |                 |
| Enable                  | `Dropdown Selection`      | F        | `Disabled`      |
| Protocol                | `ldap://`                 | T        | `ldap://`       |
| LDAP Server             | `ldap.test.com`           | T        |                 |
| Port                    | `389`                     | T        |                 |
| searchDN                | `ou=users,dc=test,dc=com` | T        |                 |
| BindDN                  | `cn=admin,dc=test,dc=com` | T        |                 |
| BindDN Password         | `ou=users,dc=test,dc=com` | T        |                 |
|                         |                           |          |                 |
| Attribute Configuration |                           |          |                 |
|                         |                           |          |                 |
| Account Field           | `uid`                     | T        | `uid`           |
| Default User Group      | `Dropdown Selection`      | F        | `Administrator` |
| Mail                    | `mail`                    | F        | `mail`          |
| Name Field              | `cn`                      | T        | `cn`            |
| Mobile Number           | `mobile`                  | F        | `mobile`        |

### Syncing Information from DingTalk to LDAP

> Reference https://github.com/anjia0532/virtual-ldap
